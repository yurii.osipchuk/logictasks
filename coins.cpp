// Description:
// Имеется 10 мешков с монетами (количество монет в каждом мешке одинаковое).
// В девяти мешках монеты золотые, а в одном - фальшивые. Вес настоящей золотой монеты 5 грамм,
// а вес фальшивой - 4 грамма. Как за одно взвешивание на весах (весы взвешивают с точностью до грамма)
// определить, в каком из мешков монеты фальшивые?

// Пронумеруем мешки от 1 до 10. Из первого мешка возьмем 1 монету, со второго 2, из третьего 3, и так до 10 монет
// (суммарно 55 монет). Произведем взвешивание этих монет. Если бы все монеты были золотыми, то весили бы 275 грамм.
// Если при нашем взвешивании не будет хватать 1 грамма, то фальшивые монеты в первом мешке, если 2-х грамм - то во втором,
// и так далее до 10-ти.


// g++ coins.cpp -o coins
#include <iostream>

class coin {
	int coinWeight{0};
public:
	int getWeight(){ return coinWeight; }
	void setWeight(int in){ coinWeight = in; }

};

int main()
{
	const int weight = 5;
	const int fakeWeight = 4;
	const int countItems = 10;
	int expectedSum = 0;
	int realSum = 0;
	coin arrayItems[countItems];

	srand((unsigned)time(nullptr));
	int item = rand() % countItems + 1;
	std::cout << "rand item: " << item << std::endl;
	for(int i = 0; i < countItems; ++i) {
		if(item == i+1) {
			arrayItems[i].setWeight(fakeWeight);
		} else {
			arrayItems[i].setWeight(weight);
		}
	}

	for(int i = 0; i < countItems; ++i) {
		expectedSum += (i+1) * weight;
		realSum += (i+1) * arrayItems[i].getWeight();
	}

	std::cout << "expectedSum: " << expectedSum << std::endl;
	std::cout << "realSum: " << realSum << std::endl;

	std::cout << "fake item: " << ((expectedSum - realSum)/(weight - fakeWeight)) << std::endl;

	return 0;
}
